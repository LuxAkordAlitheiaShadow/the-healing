# **The Healing**

<details><summary>Exercice 1 : La guérison</summary>

## Crawlers

### Spell crawler

In order to ensure data consistency, especially between spell names, we decided to recreate a spell crawler adapted to the [new website spell list](http://legacy.aonprd.com/indices/spelllists.html). We decided to implement this crawler in Python. The program creates a JSON file, `spells.json`, from the information obtained.
The information retrieved on the spells is as follows : 
- [x] Spell name (in lowercase)
- [x] URL
- [x] Short description
- [x] School (in lowercase)
- [x] Subschool(s) (in lowercase, _can be null_) 
- [x] Domain(s) (in lowercase, _can be null_)
- [x] Level per class
- [x] Components
- [x] Spell resistance (true or false)
- [x] Long description

The document created follows the format below :
```json
[
   {
      "name": "ablative barrier",
      "school": "conjuration",
      "subschool": [ "creation" ],
      "domain": [ "force" ],
      "levels": [
         {
            "class": "alchemist",
            "level": 2
         },
         {
            "class": "magus",
            "level": 2
         },
         {
            "class": "sorcerer",
            "level": 3
         },
         {
            "class": "wizard",
            "level": 3
         },
         {
            "class": "summoner",
            "level": 2
         }
      ],
      "components": [
         "V",
         "S",
         "M"
      ],
      "resistance": false,
      "longDesc": "Invisible layers of solid force surround and protect the target, granting that target a +2 armor bonus to AC. Additionally, the first 5 points of lethal damage the target takes from each attack are converted into nonlethal damage. Against attacks that already deal nonlethal damage, the target gains DR 5/\u2014. Once this spell has converted 5 points of damage to nonlethal damage per caster level (maximum 50 points), the spell is discharged.<br>",
      "shortDesc": "Surrounds the target with layers of force.",
      "url": "http://legacy.aonprd.com/ultimateCombat/spells/ablativeBarrier.html"
   },
   {
      "name": "aboleth's lung",
      "school": "transmutation",
      "subschool": [],
      "domain": [],
      "levels": [
         {
            "class": "cleric",
            "level": 2
         },
         {
            "class": "druid",
            "level": 2
         },
         {
            "class": "sorcerer",
            "level": 2
         },
         {
            "class": "wizard",
            "level": 2
         },
         {
            "class": "witch",
            "level": 2
         }
      ],
      "components": [
         "V",
         "S",
         "M/DF"
      ],
      "resistance": true,
      "longDesc": "The targets are able to breathe water, freely. However, they can no longer breathe air. Divide the duration evenly among all the creatures you touch. This spell has no effect on creatures that can already breathe water.<br>",
      "shortDesc": "Targets are able to breathe water, freely.",
      "url": "http://legacy.aonprd.com/advancedRaceGuide/uncommonRaces/gillmen.html"
   },
   ...
]
```

### Bestiary crawler
We decided to implement this crawler in Python. The program creates two JSON file, `creatureSpells.json` & `spellCreatures.json`, from the information obtained from the [bestiary page](http://legacy.aonprd.com/indices/bestiary.html) list. This page contains the information of all the other bestiary pages. The file `creatureSpells.json` lists by creature while `spellCreatures.json` lists by spells.
The information retrieved on the creatures is as follows : 
- [x] Creature name (in lowercase)
- [x] URL
- [x] Creature spell names (in lowercase) 

Are considered valid spells only those with a link towards the site core rulebook.

The document `creatureSpells.json` follows the format below :
```json
[
   {
      "name": "vilderavn",
      "spells": [
         "Modify Memory",
         "Beast Shape Iii",
         "Limited Wish",
         "Circle Of Death",
         "Alter Self",
         "Detect Thoughts",
         "True Seeing",
         "Death Knell",
         "Crushing Despair",
         "Bestow Curse"
      ],
      "url": "http://legacy.aonprd.com/bestiary5/vilderavn.html"
   },
   {
      "name": "adaro",
      "spells": [
         "Rage"
      ],
      "url": "http://legacy.aonprd.com/bestiary3/adaro.html"
   },
   ...
]
```

The document `creatureSpells.json` follows the format below :
```json
[
   {
      "spell": "Beast Shape Iii",
      "creatures": [
         {
            "name": "vilderavn",
            "url": "http://legacy.aonprd.com/bestiary5/vilderavn.html"
         },
         {
            "name": "akhlut",
            "url": "http://legacy.aonprd.com/bestiary3/akhlut.html"
         },
         {
            "name": "shen",
            "url": "http://legacy.aonprd.com/bestiary5/shen.html"
         },
         {
            "name": "toothy transmuter",
            "url": "http://legacy.aonprd.com/npcCodex/core/wizard.html"
         }
      ]
   },
   ...
]
```

### Build and run

If you have PyCharm, you can easily execute the code after importing the source code.
Otherwise, you can try the following actions.

#### Prerequisites

- Python (3.6 minimum)
- PIP

#### Installation

> ⚠️ This project, in order to work, requires the installation of several libraries : `beautifulsoup4` and `requests`. Two options are possible: install them globally on the computer or install them in a virtual environment. The interest of the virtual environment is that the libraries are not installed globally on the computer.
>
> If you want to install the libraries globally on the computer, just type the following commands:
> `pip install beautifulsoup4` and `pip install requests`
> It is also possible to install them via the file `requirements.txt` with the command: `pip install -r requirements.txt`. This last command will automatically install the necessary libraries.
>
> If you want to install the libraries in a virtual environment, you will first have to install the library allowing to create virtual environments: `virtualenv` with the following command: `pip install virtualenv`.
> You can then follow the steps below.

1. Create the virtual environment

```bash
python -m venv venv
``` 
or
```bash
python3 -m venv venv
```

2. Activate the virtual environment

On Windows :
```bash
venv/Scripts/activate      # Windows
```
On Linux :
```bash
. ./venv/Scripts/activate  # Linux
```

3. Installation of dependencies

```bash
pip install -r requirements.txt
```
or 
```bash
pip install beautifulsoup4
pip install requests
```

#### Run
```bash
python main.py
```
or
```bash
python3 main.py
```

The program `main.py` executes the spell and creature crawlers. The three files `spells.json`, `creatureSpells.json` and `spellCreatures.json` are thus created. 

## Inverted index

### Description

We are currently scrapping monster data with their spells. 
We want to make a GUI who can request from a spell name all creatures who can cast it.
So we need to invert the index of our scrapped data.

See bellow for an example :

- Scrapped data :
{ Monster A : Spell A, Spell B ; Monster B : Spell B, Spell C }
- Inverted data :
{ Spell A : Monster A; Spell B : Monster A, Monster B; Spell C : Monster B }

### Build and run

We are running the index inverter with Apache Spark 3.2.0 with Scala 2.12.15 source code.

To run the inverter index, use `inverted-index` as working directory.

Then build it :

- with `sbt run`
- or with IntelliJ builder

*Other build method may be possible, however we haven't tested them.*

After the run, the inverted JSON data saved in `.txt ` in `spellCreatures.json`.

### Clustering

According to Apache Spark, the inverter index can be run with a cluster.
You need to change the `.master` in `inverted-index/src/scala/Inverter` according to the commented line and your cluster configuration.

#### Cluster example

We tested the code in our cluster of 4 Rasperry Pi (two RPI3 and two RPI4).
This configuration give us one manager and three workers.

Bellow, a screenshot of the manager GUI.

![Cluster_Spark](Cluster_Spark.png)

</details>

<details><summary>Exercice 2 (Choix 1)</summary>

## Setup

We chose to use MongoDB in order to perform full text search and easy JSON conversion.

So in order to use our web GUI, a mongoDB instance must be running at this url 
``mongodb://localhost:27017/``

Or it can be altered by modifying `Exercice2/config.js` file. 

To install mongoDB go to : https://docs.mongodb.com/manual/installation/

Once MongoDB is installed, we recommend the use of MongoDB Compass in case database creation fails (these steps should be executed automatically) :
   - **Create the database and the collection** : You can put whatever you want it to match the `Exercice2/config.js` file, you need to put `DandD` as the database name and `spells` as the collection name.
   - **Import the data** : On MongoDB Compass, you can import data by clicking the `Add Data` button and then `import file`. Select `JSON` and then select the `Exercice2/spells.json` file.
   - **Create the text index for spells name** : In order to perform full text search, you will need to create a text index. Go to the `Indexes` section, click `Create Index` button. Then, name it whatever you want, then select the `name field` and select the `text` index type. Finally click on `Create Index`.

For our GUI we use express.

Node.Js must first be installed if it is not already : https://nodejs.org/en/download/

Then run `npm install` in `Exercice2` folder.

In order to display images, they must be installed in `Exercice2/public/Pathfinder Spellcards`.

We chose to use the link provided in the course : `https://drive.google.com/drive/folders/1B9pkMtnVuG0yuvgQb_CeOKt0w3uTfiTc`

Then launch the app with `node index.js`

The app is available at `http://localhost:8080`

## App

The app performs a lot of different queries. It is an express js app with pug for html templates. It also uses a bit of jquery. 

Spells can be filtered by : level, school, subschool, domain, spell text, component, classes and resistance.

![Search_Menu](Search_Menu.png)

The results are then displayed with these informations and the beasts having this spell are listed. An image is also included. As said before, these images come from the Drive. However, the naming conventions are not always the same. Some spells with levels like "Thought Shield III" had no image, and so truncating to "Thought Shield" would have been the best option, but others like "Beast Shape II" had no "Beast Shape" image. I chose to keep the level information when searching for an image. (This function is coded in `Exercice2/findImgUrl.js`)

![Results](Results.png)
</details>

