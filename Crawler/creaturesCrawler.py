import requests
from bs4 import BeautifulSoup
import re
import json
from tqdm import tqdm


def add_to_creature_array(_creature_array, _creature_name, _creature_spell_names, url):
    # Creates the creature object
    _creature_array.append({
        "name": _creature_name,
        "spells": list(_creature_spell_names),
        "url": url
    })



def add_to_spell_array(_spells_array, _spell_name, _creature_name, url):
    already_exists = False

    # If the spell already exists in the dictionary, adds the new creature as caster
    # (Unless that creature is already listed)
    for c in _spells_array:
        if _spell_name == c['spell']:
            already_exists = True
            if _creature_name not in c['creatures']:
                c['creatures'].append({"name": _creature_name, "url": url})
                break

    # If the spell doesn't exist, creates its object
    if not already_exists:
        _spells_array.append({
            "spell": _spell_name,
            "creatures": list([{"name": _creature_name, "url": url}])
        })


def create_file(filename, content):
    file = open(filename, "w+")
    file.write(json.dumps(content, indent=3))
    file.close()


def crawl():

    base_url = "http://legacy.aonprd.com/"
    url = "indices/bestiary.html"
    page = requests.get(base_url + url)

    # Gets page content
    soup = BeautifulSoup(page.content, "html.parser")

    # Searches for the first (and usually only) html "div" tag with id "monster-index-wrapper"
    wrapper = soup.find("div", {"id": "monster-index-wrapper"})
    # Searches for all indexes by alphabetical first letter
    indexes_by_letter = wrapper.find_all("ul")

    # Array of pages to extract information from
    creature_pages = []
    # Array of creatures, with their name and spells
    creature_spells = []
    # Array of spells, with their name and creatures
    spell_creatures = []

    print("[CREATURES] Beginning to extract links.")

    for _index in indexes_by_letter:
        # Searches for all creatures in each index
        creature_items = _index.find_all("li")

        for creatureItem in creature_items:
            # Searches for the first (and usually only) creature link for each spell
            # and get the that creature page content
            _creatureLink = creatureItem.find("a")
            creature_pages.append(re.sub("#.*", "", _creatureLink["href"].replace("../", base_url)))

    print("[CREATURES] Links successfully extracted.")
    print("[CREATURES] Beginning to extract information.")

    creature_pages = set(creature_pages)

    for page in tqdm(creature_pages, colour="blue"):
        _creaturePage = requests.get(page)
        _creaturePageBS = BeautifulSoup(_creaturePage.content, "html.parser")

        for block in _creaturePageBS.find_all("p", {"class": "stat-block-title"}):
            creature_name = ""
            creature_spell_names = set()

            # Isolates the creature name
            # If the creature is already known, continue
            try:
                _creatureTitle = re.sub("CR.*", "", block.text)
                creature_name = _creatureTitle.strip().lower()
                if any(c for c in creature_spells if c['name'] == creature_name):
                    continue
            except:
                print(f"Error with creature title : {page}")

            for sibling in block.find_next_siblings(): # "p"):
                if sibling is None:
                    continue
                _sibling_classes = sibling.get('class')
                if _sibling_classes is not None and "stat-block-title" in _sibling_classes:
                    break
                if sibling.name[0] == 'h':
                    break
                else:
                    # Lists the spells that can be used by this creature (without duplicates)
                    # Are only considered valid spells those who have a link to the "spells" section
                    try:
                        _creatureSpellLinks = sibling.find_all(
                            "a",
                            {"href": re.compile(r'.*coreRulebook/spells/.*')}
                        )
                        creature_spell_names = set.union(
                            creature_spell_names,
                            set(map(lambda _spell: _spell.text.strip().lower(), _creatureSpellLinks))
                        )
                    except:
                        print(f"Error with creature spells : {page}")

            # Adds data to the array of creatures
            add_to_creature_array(creature_spells, creature_name, creature_spell_names, page)

            # Adds data to the array of spells
            for _spell in creature_spell_names:
                add_to_spell_array(spell_creatures, _spell, creature_name, page)

    print("[CREATURES] Information successfully extracted.")
    print("[CREATURES] Beginning to write JSON files.")

    # Save creatures data in a json file
    create_file("creatureSpells.json", creature_spells)
    # Save spells data in a json file
    create_file("spellCreatures.json", spell_creatures)

    print("[CREATURES] JSON files successfully created.")
    print("[CREATURES] End of crawl.")
