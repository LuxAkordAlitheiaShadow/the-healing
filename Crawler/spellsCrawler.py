import math
import traceback
from tqdm import tqdm
import requests
from bs4 import BeautifulSoup
import re
import json


def create_file(filename, content):
    file = open(filename, "w+")
    file.write(json.dumps(content, indent=3))
    file.close()


def regex_search(regex, string):
    result = re.search(regex, string)
    if result is not None:
        return result.group(0).strip()
    else:
        return None


def crawl():
    base_url = "http://legacy.aonprd.com/"
    url = "indices/spelllists.html"
    page = requests.get(base_url + url)

    # Gets page content
    soup = BeautifulSoup(page.content, "html.parser")

    # Searches for the first (and usually only) html "table" tag with id "spelllist"
    table = soup.find("table", {"id": "spelllist"})

    print("[SPELLS] Beginning to extract links.")

    # Searches for all table lines
    lines = table.find("tbody").find_all("tr")

    # Array of pages to extract information from
    pages = []
    # Array of spells
    spells = []

    for line in lines:
        # Extract information of each line
        _cells = line.find_all("td")
        _name = _cells[1].find("a").text.lower()
        _url = re.sub("#.*", "", _cells[1].find("a")["href"].replace("../", base_url))
        _school = _cells[2].text
        _short_desc = _cells[3].text
        pages.append({
            "name": _name,
            "url": _url,
            "school": _school,
            "short_desc": _short_desc
        })

    print("[SPELLS] Links successfully extracted.")
    print("[SPELLS] Beginning to extract information.")

    for page in tqdm(pages, colour="green"):
        _spellPage = requests.get(page["url"])
        _spellPageBS = BeautifulSoup(_spellPage.content, "html.parser")
        spell_name = ""

        for block in _spellPageBS.find_all("p", {"class": "stat-block-title"}):
            is_a_spell = False
            spell = {}
            spell_long_description = ""
            
            # Isolates the spell name
            # If the spell is already known, continue
            try:
                spell_name = block.text.strip().lower()
                # URL
                spell_url = next((p['url'] for p in pages if p['name'].lower() == spell_name), None)
                spell['url'] = spell_url
                
                #gestion des greater, lesser toussa pour ensuite les lier avec les créatures
                keyword = regex_search(", *lesser|, *greater|, *mass", spell_name)
                if keyword is not None :
                    keyword = re.sub(",","", keyword)
                    keyword = keyword.strip()

                    spell_name = re.sub(", *lesser|, *greater|, *mass", "", spell_name)
                    spell_name = keyword + " " + spell_name

                if any(c for c in spells if c['name'] == spell_name):
                    continue
                spell['name'] = spell_name
            except Exception as spellException:
                print(f"Error with spell name : {page['url']}")
                print(traceback.format_exc())

            for index, sibling in enumerate(block.find_next_siblings("p")):
                if sibling is None:
                    continue
                if sibling.get('class') is not None and "stat-block-title" in sibling.get('class'):
                    break
                else:
                    _isSchool = None
                    # Extracts spell information
                    try:
                        # If there's no school at first, it isn't a spell (an item for instance)
                        _isSchool = re.search("School.*", sibling.text)
                        if index == 0 and _isSchool is None:
                            break
                        is_a_spell = True

                        if _isSchool is not None:
                            # School
                            spell_school = regex_search("(?<=School).*(?=\()", sibling.text)
                            if spell_school is None:
                                spell_school = regex_search("(?<=School).*(?=\[)", sibling.text)
                            if spell_school is None:
                                spell_school = regex_search("(?<=School).*(?=;)", sibling.text)
                            if spell_school is None:
                                spell_school = regex_search("(?<=School).*(?=;)", sibling.text)
                            if spell_school is None:
                                spell_school = regex_search("(?<=School).*(?=Level)", sibling.text)
                            _spell_subschool = regex_search("(?<=\()(calling|shadow|creation|healing|summoning|teleportation|scrying|charm|compulsion|figment|glamer|pattern|phantasm|shadow|,| |or)*(?=\))", sibling.text)
                            spell_subschool = list(map(lambda el: el.strip(), _spell_subschool.split(',| or '))) \
                                if _spell_subschool is not None else list()
                            if len(spell_subschool) == 0:  # Site formatting error
                                _spell_domains = regex_search("(?<=\()(.*(?=\)))", sibling.text)
                            else:
                                _spell_domains = regex_search("(?<=\[)(.*(?=\]))", sibling.text)
                            spell_domains = list(map(lambda el: re.sub("mind affecting", "mind-affecting",
                                                                       re.sub("language dependent", "language-dependent", el)).strip(),
                                                     _spell_domains.split(','))) \
                                if _spell_domains is not None else list()
                            spell['school'] = spell_school
                            spell['subschool'] = spell_subschool
                            spell['domain'] = spell_domains
                    except Exception as spellException:
                        print(f"Problème with spell school : {page}")
                        print(traceback.format_exc())

                    try:
                        if _isSchool is not None:
                            # Levels
                            _spellLevels = regex_search("(?<=Level).*", sibling.text)
                            if _spellLevels is not None:
                                _spellLevels = re.sub("\(.*\)", "", re.sub("\s+", " ", _spellLevels))
                                _spellLevels = re.findall("([a-zA-Z]+[a-zA-Z/ -]+ [0-9]+)+", _spellLevels)

                                # Creates object with the level associated with each class
                                spell_levels = []
                                _spellLevelsList = list(map(lambda el: [el.strip()[:-2], el.strip()[-1]], _spellLevels))
                                for level in _spellLevelsList:
                                    character_classes = level[0].split('/')
                                    for characterClass in character_classes:
                                        spell_levels.append({
                                            "class": characterClass.strip().lower(),
                                            "level": int(level[1])
                                        })
                                spell['levels'] = spell_levels
                    except Exception as spellException:
                        print(f"Problème with spell levels : {page}")
                        print(traceback.format_exc())

                    try:
                        _isComponents = regex_search("Component(s)?.*", sibling.text)
                        if _isComponents is not None:
                            # Components
                            _spellComponents = regex_search("(?<=Components).*", sibling.text)
                            if _spellComponents is not None:
                                _spellComponents = re.sub("\(.*\)", "", re.sub("\s+", "", _spellComponents))
                                spell_components = re.findall("(AF|[VSFMD]+[/VSFMD]*)(?=[ ,;<(]|$)", _spellComponents)
                                spell['components'] = spell_components
                    except Exception as spellException:
                        print(f"Problème with spell components : {page}")
                        print(traceback.format_exc())

                    try:
                        _isResistance = re.search(".*Spell Resistance.*", sibling.text)
                        if _isResistance is not None:
                            # Resistance
                            _spell_resistance = regex_search("(?<=Spell Resistance).*", sibling.text)
                            spell_resistance = True if ("yes" in _spell_resistance.lower()) else False
                            spell['resistance'] = spell_resistance
                    except Exception as spellException:
                        print(f"Problème with spell resistance : {page}")
                        print(traceback.format_exc())

                    if not sibling.has_attr('class') or sibling['class'][0] == 'stat-block-2' or sibling['class'][0] == 'stat-block' :
                        # Long description
                        spell_long_description = spell_long_description + sibling.text + "<br>"
                        spell['longDesc'] = spell_long_description
            # Short description
            spell_short_description = next((p['short_desc'] for p in pages if p['name'].lower() == spell_name), None)
            spell['shortDesc'] = spell_short_description
            

            # Adds the spell to the list
            if is_a_spell:
                spells.append(spell)

    print("[SPELLS] Information successfully extracted.")
    print("[SPELLS] Beginning to write JSON files.")

    # Save spells data in a json file
    create_file("spells.json", spells)

    print("[SPELLS] JSON files successfully created.")
    print("[SPELLS] End of crawl.")
