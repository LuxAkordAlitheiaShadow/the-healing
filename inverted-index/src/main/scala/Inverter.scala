import scala.collection.mutable
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql._
import org.apache.spark.sql.functions._

object Inverter extends App
{

  //Pas de messages rouges partout
  Logger.getLogger("org").setLevel(Level.ERROR)

  // Initialisation de la session
  val sparkSession = SparkSession
    .builder()
    .appName("Inverted index for scrapped data (spells and monsters)")
    .master("local[*]")
    .config("spark.sql.jsonGenerator.ignoreNullFields","false")
    .getOrCreate()

  val sparkContext = sparkSession.sparkContext

  // Reading JSON data
  val df = sparkSession.read.option("multiline", "true").json("creatureSpells.json")

  import sparkSession.implicits._

  //Inversion des sorts et des creatures
  val spellCreatures =
    df.select($"name", explode($"spells")
    ).withColumnRenamed("col", "spell").withColumnRenamed("name","creatures")
      .groupBy("spell").agg(collect_list("creatures").as("creatures"))

  //Affichage
  spellCreatures.collect().foreach(el => println(Console.WHITE + "Spell : " + Console.BLUE + el(0) + Console.WHITE + " Creatures : " + Console.GREEN + el(1).asInstanceOf[mutable.WrappedArray[String]].mkString(", ")))

  //Enregistrement en JSON multiline (enregistrement spark par défaut en json lines)
  spellCreatures.agg(to_json(collect_list(struct(col("spell"),col("creatures")))).alias("d")).write.mode(SaveMode.Overwrite).text("spellCreatures.json")
}
