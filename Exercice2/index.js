const express = require('express')

const mongoose = require('mongoose')

var config = require('./config');

const findImgUrl = require('./findImgUrl')

var MongoClient = require('mongodb').MongoClient;

console.log(config)
const dbName = config.mongo.dbName;
const collectionName = config.mongo.collectionName;
var url = config.mongo.url;

let app = express();
let port = 8080;

app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.set("views", "./views");
app.set("view engine", "pug");
app.use(express.static("./public")); 


app.locals.findImgURL = findImgUrl;


app.listen(port, () =>  { // ecoute du serveur sur le port 8080
    var fs = require('fs');
    var mydocuments = fs.readFile('spells.json', 'utf8', function (err, data) {
        MongoClient.connect(url, function(err, db) {
            if (err) throw err;
            console.log("Database accessed!");
        });
        MongoClient.connect(url, function(err, db) {
            if (err) throw err;
            var dbo = db.db(dbName);
            dbo.listCollections({name: collectionName})
                .next(function(err, collinfo) {
                    if (!collinfo) {
                        //Si n'existe pas on crée et on insere
                        dbo.collection(collectionName).insertMany(JSON.parse(data), function(err, docs) {
                            dbo.collection(collectionName).createIndex({ spellText: "text" }, function(err, docs) {
                                console.log(err)
                                console.log("Index created")
                                db.close()
                            })
                            
                        });
                    }
            });
            
            //dbo.collection(collectionName).distinct("domain",function(err, result) {console.log("dednas");console.log(result);console.log(err);db.close()})

        
        });
    });
    console.log('le serveur fonctionne')
})

app.get('/', function(req, res) { // création de la route sous le verbe get
    res.render("index", {title : "Home"});
})

app.post('/ajax', function(req, res){
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        res.locals.findImgUrl = findImgUrl;
        var dbo = db.db(dbName);
        console.log("request = " + req.body.request)
        if (req.body.request != "") {
            var query = eval('(' + req.body.request + ')');
            dbo.collection(collectionName).find(query).toArray(function(err, result) {
                if (err) throw err;
                console.log(result)

                res.render("resultList", {infos : result});
                db.close();
            });
        } else {
            res.render("resultList", {infos : []});
        }

    });
    
});
 
 
