

module.exports =  function(spellName) {
    const baseURL = "/Pathfinder Spellcards/";
    const endURL = ".png";

    //Supression des chiffres romains
    
    // var re = / +[i|v]+$/g;
    // spellName = spellName.replace(re, "");
    
    //Mise en forme des chiffres romains
    var re = / +[i|v]+$/g;
    var numbers = spellName.match(re);
    if (numbers != null) {
        spellName = spellName.replace(re, numbers[0].toUpperCase());

    }
    
    

    //Inversion des greaters vis a vis des noms d'images
    re = /^(greater|mass|lesser) */g;
    var result = spellName.match(re);

    if (result != null) {
        spellName = spellName.replace(result, "")
        spellName = spellName + ", " + result[0].trim();

    }

    const url = baseURL + spellName + endURL;
    return url;

}