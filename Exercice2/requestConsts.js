

const classAlchemistRequest = "{levels : {$elemMatch:{class: 'alchemist'}}}"
const classAntipaladinRequest = "{levels : {$elemMatch:{class: 'antipaladin'}}}"
const classBardRequest = "{levels : {$elemMatch:{class: 'bard'}}}"
const classBloodragerRequest = "{levels : {$elemMatch:{class: 'bloodrager'}}}"
const classClericRequest = "{levels : {$elemMatch:{class: 'cleric'}}}"
const classOracleRequest = "{levels : {$elemMatch:{class: 'oracle'}}}"
const classDruidRequest = "{levels : {$elemMatch:{class: 'druid'}}}"
const classElementalistwizardRequest = "{levels : {$elemMatch:{class: 'elementalist wizard'}}}"
const classInquisitorRequest = "{levels : {$elemMatch:{class: 'inquisitor'}}}"
const classMagusRequest = "{levels : {$elemMatch:{class: 'magus'}}}"
const classMediumRequest = "{levels : {$elemMatch:{class: 'medium'}}}"
const classMesmeristRequest = "{levels : {$elemMatch:{class: 'mesmerist'}}}"
const classOccultistRequest = "{levels : {$elemMatch:{class: 'occultist'}}}"
const classPaladinRequest = "{levels : {$elemMatch:{class: 'paladin'}}}"
const classPsychicRequest = "{levels : {$elemMatch:{class: 'psychic'}}}"
const classRangerRequest = "{levels : {$elemMatch:{class: 'ranger'}}}"
const classShamanRequest = "{levels : {$elemMatch:{class: 'shaman'}}}"
const classSorcererRequest = "{levels : {$elemMatch:{class: 'sorcerer'}}}"
const classWizardRequest = "{levels : {$elemMatch:{class: 'wizard'}}}"
const classSpiritualistRequest = "{levels : {$elemMatch:{class: 'spiritualist'}}}"
const classSummonerRequest = "{levels : {$elemMatch:{class: 'summoner'}}}"
const classWitchRequest = "{levels : {$elemMatch:{class: 'witch'}}}"

const resistanceTrueRequest = "{resistance : true}"
const resistanceFalseRequest = "{resistance : false}"

const spellTextSearchRequestStart = '{ $text: { $search: "\\\"'
const spellTextSearchRequestEnd = '\\\"" } }'

const nameSearchRequestStart = '{"name" : /.*'
const nameSearchRequestEnd = '.*/i}'

const componentsRequestStart = "{ components : { $all : [";
const componentsRequestEnd = "]}}";

const schoolAbjurationRequest = "{'school':'abjuration'}"
const schoolConjurationRequest = "{'school':'conjuration'}"
const schoolDivinationRequest = "{'school':'divination'}"
const schoolEnchantmentRequest = "{'school':'enchantment'}"
const schoolEvocationRequest = "{'school':'evocation'}"
const schoolIllusionRequest = "{'school':'illusion'}"
const schoolNecromancyRequest = "{'school':'necromancy'}"
const schoolTransmutationRequest = "{'school':'transmutation'}"
const schoolUniversalRequest = "{'school':'universal'}"

const subSchoolCallingRequest = "{'subSchool':'calling'}"
const subSchoolCreationRequest = "{'subSchool':'creation'}"
const subSchoolHealingRequest = "{'subSchool':'healing'}"
const subSchoolSummoningRequest = "{'subSchool':'summoning'}"
const subSchoolTeleportationRequest = "{'subSchool':'teleportation'}"
const subSchoolScryingRequest = "{'subSchool':'scrying'}"
const subSchoolCharmRequest = "{'subSchool':'charm'}"
const subSchoolCompulsionRequest = "{'subSchool':'compulsion'}"
const subSchoolFigmentRequest = "{'subSchool':'figment'}"
const subSchoolGlamerRequest = "{'subSchool':'glamer'}"
const subSchoolPatternRequest = "{'subSchool':'pattern'}"
const subSchoolPhantasmRequest = "{'subSchool':'phantasm'}"
const subSchoolShadowRequest = "{'subSchool':'shadow'}"
const subSchoolPolymorphRequest = "{'subSchool':'polymorph'}"




