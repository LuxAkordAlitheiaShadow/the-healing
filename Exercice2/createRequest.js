


$(document).ready(function(){
    $("form#spellSearchForm").on('submit', function(e){
        e.preventDefault();
        var request = ""
        request = createRequest()
        json = JSON.stringify({"request" : request})
        if (request != "") {
            $.ajax({
                type: 'post',
                url: '/ajax',
                data: json,
                contentType: "application/json",
            })
            .done(function(data){
                $('.Result').html(data);
                minimizeSearchMenu()
            });
        }
        
        
    });
    // Execute a function when the user releases a key on the keyboard
    window.addEventListener("keydown", function(event) {
        if (event.key == "Enter") {
        // Cancel the default action, if needed
        event.preventDefault();
    
        // Trigger the button element with a click
        $("form#spellSearchForm").submit();
        }
    });

    $("#ButtonPlus").on('click', function(e){
        e.preventDefault();
        maximizeSearchMenu()
    });
});

minimizeSearchMenu = function(){
    const titleDiv = $("#TitleDiv")
    const bannerDiv = $('#MainBanner')
    const buttonDiv = $("#ButtonPlusDiv")
    const resultDiv = $(".Result")
    bannerDiv.css("height","8%")
    bannerDiv.css("position","fixed")
    resultDiv.css("margin-top", bannerDiv.height() + 50)
    titleDiv.css("height","100%")
    titleDiv.css("margin-top","2%")
    buttonDiv.show()
}

maximizeSearchMenu = function() {
    const titleDiv = $("#TitleDiv")
    const bannerDiv = $('#MainBanner')
    const buttonDiv = $("#ButtonPlusDiv")
    const resultDiv = $(".Result")
    bannerDiv.css("height","auto")
    bannerDiv.css("position","static")
    resultDiv.css("margin-top", 50)
    titleDiv.css("height","auto")
    titleDiv.css("margin-top","auto")
    buttonDiv.hide()
    window.scrollTo(0, 0);
}

createRequest =  function() {
    var request = ""

    request = createClassRequest(request);

    request = createResistanceRequest(request);

    request = createSpellNameRequest(request);

    request = createComponentRequest(request);

    request = createSpellTextRequest(request);

    request = createSchoolRequest(request);

    request = createSubSchoolRequest(request);

    request = createLevelRequest(request);

    request = createDomainRequest(request)


    if (request != ""){
        request = addOperator(request, "and");
    }
    
    return request
};

function createDomainRequest(request) {
    const domainSelect = document.forms["spellSearchForm"]["domain-select"];
    const operator = document.forms["spellSearchForm"]["domain-operator"].value;
    const domainSelectValues = getValuesFromSelect(domainSelect)
    var tempRequest = ""
    domainSelectValues.forEach(e =>  tempRequest = `{domain : "${e}"}` + ", " + tempRequest )
    
    if (tempRequest != "") {
        tempRequest = removeUnnecessaryComma(tempRequest)
        tempRequest = addOperator(tempRequest, operator)
        request = tempRequest + ", " + request
    }

    return request
}

function getValuesFromSelect(selectElement) {
    var selectedList = [];
    for (var i = 0; i < selectElement.length; i++) {
        if (selectElement.options[i].selected) selectedList.push(selectElement.options[i].value);
    }
    return selectedList
}

function createLevelRequest(request) {
    const spellLevelMin = document.forms["spellSearchForm"]["spellLevelMin"].value;
    const spellLevelMax = document.forms["spellSearchForm"]["spellLevelMax"].value;

    const levelRequest = `{levels : {$elemMatch:{level: {$gte : ${spellLevelMin}, $lte : ${spellLevelMax}}}}}`

    return levelRequest + "," + request
}

function createSpellNameRequest(request) {

    const spellName = document.forms["spellSearchForm"]["spellName"].value;

    if (spellName != "") {
        request = nameSearchRequestStart + spellName + nameSearchRequestEnd + ", " + request;
        request = removeUnnecessaryComma(request);
    }
    return request
}

function createSpellTextRequest(request) {

    const spellText = document.forms["spellSearchForm"]["spellText"].value;
    
    spellText.trim()

    if (spellText != "") {
        request = spellTextSearchRequestStart + spellText + spellTextSearchRequestEnd + ", " + request;
        request = removeUnnecessaryComma(request);
    }

    return request
}

function createComponentRequest(request) {

    var components = document.forms["spellSearchForm"]["components"].value;

    const re = new RegExp(" +","g")
    components = '"' + components.trim() + '"'
    components = components.replaceAll(re,'\", \"')
    if (components != '""') {
        request = componentsRequestStart + components + componentsRequestEnd + ", " + request;
        request = removeUnnecessaryComma(request);
    }

    return request
}

function createClassRequest(classRequest){

    const isAlchemist = document.forms["spellSearchForm"]["level-alchemist"].checked;
    const isAntipaladin = document.forms["spellSearchForm"]["level-antipaladin"].checked;
    const isBard = document.forms["spellSearchForm"]["level-bard"].checked;
    const isBloodrager = document.forms["spellSearchForm"]["level-bloodrager"].checked;
    const isCleric = document.forms["spellSearchForm"]["level-cleric"].checked;
    const isOracle = document.forms["spellSearchForm"]["level-oracle"].checked;
    const isDruid = document.forms["spellSearchForm"]["level-druid"].checked;
    const isElementalistWizard = document.forms["spellSearchForm"]["level-elementalistwizard"].checked;
    const isInquisitor = document.forms["spellSearchForm"]["level-inquisitor"].checked;
    const isMagus = document.forms["spellSearchForm"]["level-magus"].checked;
    const isMedium = document.forms["spellSearchForm"]["level-medium"].checked;
    const isMesmerist = document.forms["spellSearchForm"]["level-mesmerist"].checked;
    const isOccultist = document.forms["spellSearchForm"]["level-occultist"].checked;
    const isPaladin = document.forms["spellSearchForm"]["level-paladin"].checked;
    const isPsychic = document.forms["spellSearchForm"]["level-psychic"].checked;
    const isRanger = document.forms["spellSearchForm"]["level-ranger"].checked;
    const isShaman = document.forms["spellSearchForm"]["level-shaman"].checked;
    const isSorcerer = document.forms["spellSearchForm"]["level-sorcerer"].checked;
    const isWizard = document.forms["spellSearchForm"]["level-wizard"].checked;
    const isSpiritualist = document.forms["spellSearchForm"]["level-spiritualist"].checked;
    const isSummoner = document.forms["spellSearchForm"]["level-summoner"].checked;
    const isWitch = document.forms["spellSearchForm"]["level-witch"].checked;

    const operator = document.forms["spellSearchForm"]["level-operator"].value;

    classRequest = addFilter(classRequest, isAlchemist, classAlchemistRequest)
    classRequest = addFilter(classRequest, isAntipaladin, classAntipaladinRequest)
    classRequest = addFilter(classRequest, isBard, classBardRequest)
    classRequest = addFilter(classRequest, isBloodrager, classBloodragerRequest)
    classRequest = addFilter(classRequest, isCleric, classClericRequest)
    classRequest = addFilter(classRequest, isOracle, classOracleRequest)
    classRequest = addFilter(classRequest, isDruid, classDruidRequest)
    classRequest = addFilter(classRequest, isElementalistWizard, classElementalistwizardRequest)
    classRequest = addFilter(classRequest, isInquisitor, classInquisitorRequest)
    classRequest = addFilter(classRequest, isMagus, classMagusRequest)
    classRequest = addFilter(classRequest, isMedium, classMediumRequest)
    classRequest = addFilter(classRequest, isMesmerist, classMesmeristRequest)
    classRequest = addFilter(classRequest, isOccultist, classOccultistRequest)
    classRequest = addFilter(classRequest, isPaladin, classPaladinRequest)
    classRequest = addFilter(classRequest, isPsychic, classPsychicRequest)
    classRequest = addFilter(classRequest, isRanger, classRangerRequest)
    classRequest = addFilter(classRequest, isShaman, classShamanRequest)
    classRequest = addFilter(classRequest, isSorcerer, classSorcererRequest)
    classRequest = addFilter(classRequest, isWizard, classWizardRequest)
    classRequest = addFilter(classRequest, isSpiritualist, classSpiritualistRequest)
    classRequest = addFilter(classRequest, isSummoner, classSummonerRequest)
    classRequest = addFilter(classRequest, isWitch, classWitchRequest)
    
    
    classRequest.trim()

    if (classRequest != "") {
        classRequest = removeUnnecessaryComma(classRequest)
        classRequest = addOperator(classRequest, operator)
    }

    return classRequest
}

function createSchoolRequest(schoolRequest){

    const isAbjuration = document.forms["spellSearchForm"]["school-abjuration"].checked;
    const isConjuration = document.forms["spellSearchForm"]["school-conjuration"].checked;
    const isDivination = document.forms["spellSearchForm"]["school-divination"].checked;
    const isEnchantment = document.forms["spellSearchForm"]["school-enchantment"].checked;
    const isEvocation = document.forms["spellSearchForm"]["school-evocation"].checked;
    const isIllusion = document.forms["spellSearchForm"]["school-illusion"].checked;
    const isNecromancy = document.forms["spellSearchForm"]["school-necromancy"].checked;
    const isTransmutation = document.forms["spellSearchForm"]["school-transmutation"].checked;
    const isUniversal = document.forms["spellSearchForm"]["school-universal"].checked;

    const operator = document.forms["spellSearchForm"]["school-operator"].value;
    var tempRequest = "";
    tempRequest = addFilter(tempRequest, isAbjuration, schoolAbjurationRequest)
    tempRequest = addFilter(tempRequest, isConjuration, schoolConjurationRequest)
    tempRequest = addFilter(tempRequest, isDivination, schoolDivinationRequest)
    tempRequest = addFilter(tempRequest, isEnchantment, schoolEnchantmentRequest)
    tempRequest = addFilter(tempRequest, isEvocation, schoolEvocationRequest)
    tempRequest = addFilter(tempRequest, isIllusion, schoolIllusionRequest)
    tempRequest = addFilter(tempRequest, isNecromancy, schoolNecromancyRequest)
    tempRequest = addFilter(tempRequest, isTransmutation, schoolTransmutationRequest)
    tempRequest = addFilter(tempRequest, isUniversal, schoolUniversalRequest)
    
    tempRequest.trim()

    if (tempRequest != "") {
        tempRequest = removeUnnecessaryComma(tempRequest);
        tempRequest = addOperator(tempRequest, operator)
        if (schoolRequest != "") {
            schoolRequest = schoolRequest + "," + tempRequest;
        } else {
            schoolRequest = tempRequest;
        }
        
    }

    return schoolRequest
}

function createSubSchoolRequest(subSchoolRequest){

    const isCalling = document.forms["spellSearchForm"]["subschool-calling"].checked;
    const isCreation = document.forms["spellSearchForm"]["subschool-creation"].checked;
    const isHealing = document.forms["spellSearchForm"]["subschool-healing"].checked;
    const isSummoning = document.forms["spellSearchForm"]["subschool-summoning"].checked;
    const isTeleportation = document.forms["spellSearchForm"]["subschool-teleportation"].checked;
    const isScrying = document.forms["spellSearchForm"]["subschool-scrying"].checked;
    const isCharm = document.forms["spellSearchForm"]["subschool-charm"].checked;
    const isCompulsion = document.forms["spellSearchForm"]["subschool-compulsion"].checked;
    const isFigment = document.forms["spellSearchForm"]["subschool-figment"].checked;
    const isGlamer = document.forms["spellSearchForm"]["subschool-glamer"].checked;
    const isPattern = document.forms["spellSearchForm"]["subschool-pattern"].checked;
    const isPhantasm = document.forms["spellSearchForm"]["subschool-phantasm"].checked;
    const isShadow = document.forms["spellSearchForm"]["subschool-shadow"].checked;
    const isPolymorph = document.forms["spellSearchForm"]["subschool-polymorph"].checked;

    const operator = document.forms["spellSearchForm"]["subschool-operator"].value;

    var tempRequest = "";
    tempRequest = addFilter(tempRequest, isCalling, subSchoolCallingRequest)
    tempRequest = addFilter(tempRequest, isCreation, subSchoolCreationRequest)
    tempRequest = addFilter(tempRequest, isHealing, subSchoolHealingRequest)
    tempRequest = addFilter(tempRequest, isSummoning, subSchoolSummoningRequest)
    tempRequest = addFilter(tempRequest, isTeleportation, subSchoolTeleportationRequest)
    tempRequest = addFilter(tempRequest, isScrying, subSchoolScryingRequest)
    tempRequest = addFilter(tempRequest, isCharm, subSchoolCharmRequest)
    tempRequest = addFilter(tempRequest, isCompulsion, subSchoolCompulsionRequest)
    tempRequest = addFilter(tempRequest, isFigment, subSchoolFigmentRequest)
    tempRequest = addFilter(tempRequest, isGlamer, subSchoolGlamerRequest)
    tempRequest = addFilter(tempRequest, isPattern, subSchoolPatternRequest)
    tempRequest = addFilter(tempRequest, isPhantasm, subSchoolPhantasmRequest)
    tempRequest = addFilter(tempRequest, isShadow, subSchoolShadowRequest)
    tempRequest = addFilter(tempRequest, isPolymorph, subSchoolPolymorphRequest)
    
    
    tempRequest.trim()

    if (tempRequest != "") {
        tempRequest = removeUnnecessaryComma(tempRequest);
        tempRequest = addOperator(tempRequest, operator)
        if (subSchoolRequest != "") {
            subSchoolRequest = subSchoolRequest + "," + tempRequest;
        } else {
            subSchoolRequest = tempRequest;
        }
    }

    return subSchoolRequest
}


function createResistanceRequest(resistanceRequest){

    const resistance = document.forms["spellSearchForm"]["resistance"].value;
    switch (resistance) {
        case "True" :
            resistanceRequest = resistanceTrueRequest + ", " + resistanceRequest;
            resistanceRequest = removeUnnecessaryComma(resistanceRequest)
            break;
        case "False" : 
            resistanceRequest = resistanceFalseRequest + ", " + resistanceRequest;
            resistanceRequest = removeUnnecessaryComma(resistanceRequest)
            break;
    }
    return resistanceRequest
}

function addFilter(request, value, constRequest) {
    if (value) {
        request = constRequest + ", " + request; 
    }
    return request
}

function addOperator(request, operator) {
    return "{ $" + operator +" : [" + request + "]}"
}

function removeUnnecessaryComma(classRequest) {
    var re = new RegExp(", *$");
    return classRequest.replace(re, "")
}

