var config = {};

config.mongo = {};

config.mongo.url = "mongodb://localhost:27017/";
config.mongo.dbName = "DandD";
config.mongo.collectionName = "spells";

module.exports = config;